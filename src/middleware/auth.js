import jwt from "jsonwebtoken";
import config from "../config";

let checkToken = (req, res, next) => {
  let token = req.headers["x-access-token"] || req.headers["authorization"]; // Express headers are auto converted to lowercase
  if (token.startsWith("Bearer ")) {
    //console.log("here");
    // Remove Bearer from string
    token = token.split(" ");
    //console.log("token", token);
    token = token[1];
  }
  if (token) {
    jwt.verify(token, config.SECRET, (err, decoded) => {
      // change decoded to user
      if (err) {
        // console.log(err);

        return res.json({
          success: false,
          message: "Token is not valid"
        });
      } else {
        req.decoded = decoded;
        next();
      }
    });
  } else {
    return res.json({
      success: false,
      message: "Auth token is not supplied"
    });
  }
};

module.exports = {
  checkToken: checkToken
};
