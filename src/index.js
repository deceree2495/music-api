import express from "express";
import session from "express-session";
import http from "http";
import bodyParser from "body-parser";
import mongoose from "mongoose";
import passport from "passport";
const LocalStrategy = require("passport-local").Strategy;

import config from "./config";
import routes from "./routes";

let app = express();
app.server = http.createServer(app);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static("public")); // for multer
app.use(
  bodyParser.json({
    limit: config.bodyLimit
  })
);
app.use(
  session({
    secret: config.SECRET,
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true }
  })
);

// app.use(passport.initialize());
// const User = require("./models/user");
// passport.use(
//   new LocalStrategy(
//     {
//       usernameField: "email",
//       passwordField: "password"
//     },
//     User.authenticate()
//   )
// );

// passport.serializeUser(User.serializeUser());
// passport.deserializeUser(User.deserializeUser());

app.use("/v1", routes);

app.server.listen(config.port);
console.log(`Started on port ${app.server.address().port}`);

export default app;
