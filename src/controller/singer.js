import { Router } from "express";
import Singer from "../models/singer";
import { checkToken } from "../middleware/auth";

export default ({ config, db }) => {
  let api = Router();

  api.get("/", async (req, res) => {
    try {
      let singer = await Singer.find();
      res.status(200).json({ success: true, data: singer });
    } catch (err) {
      res.status(500).json({ success: false, message: err.message });
      console.log(err);
    }
  });

  api.post("/addSinger", async (req, res) => {
    console.log(req.body.name);

    let newSinger = new Singer();
    newSinger.name = req.body.name;
    newSinger.createdAt = new Date();

    newSinger.save(err => {
      if (err) {
        res.status(400).json({ message: err.message });
      }
      res.json(newSinger);
    });
  });

  return api;
};
