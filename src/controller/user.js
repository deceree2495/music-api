import mongoose from "mongoose";
import { Router } from "express";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

import config from "../config";
import User from "../models/user";
import Token from "../models/token";
import { checkToken } from "../middleware/auth";

export default ({ config, db }) => {
  let api = Router();

  api.post("/register", (req, res) => {
    // console.log(req.body);
    const newUser = new User({
      name: {
        firstName: req.body.firstName,
        lastName: req.body.lastName
      },
      email: req.body.email,
      password: req.body.password, // encrypt, = 1234567
      createdAt: new Date()
    });

    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(newUser.password, salt, (err, hash) => {
        if (err) {
          console.log(err);
        }
        newUser.password = hash;
        console.log({ newUser });

        newUser.save(err => {
          console.log(err);

          if (!err) {
            res.status(201).json({ success: true, result: newUser });
          } else {
            res.status(500).json({ success: false, message: err.message });
          }
        });
      });
    });
  });

  api.post("/login", async (req, res) => {
    const user = {
      email: req.body.email,
      password: req.body.password
    };

    const registeredUser = await User.findOne({ email: user.email });

    if (!registeredUser.email === user.email) {
      req.session.user = registeredUser;
      // console.log(req.session.user);

      throw res.status(401).send({
        success: false,
        message: "Authentication failed. User not found."
      });
    }

    bcrypt.compare(user.password, registeredUser.password, (err, isMatch) => {
      if (err) {
        res.status(401).send({
          success: false,
          message: "Authentication failed. Wrong password."
        });
      }
      if (isMatch) {
        // should be a data that can be save to token?
        // const obj = {
        //   _id: registeredUser._id,
        //   name: registeredUser.name,
        //   createdAt: registeredUser.createdAt,
        //   loginAt: new Date()
        // };
        // generate!
        const generateToken = new Token({ user: registeredUser._id });
        // console.log({ generateToken });
        // console.log(typeof generateToken);

        let token = jwt.sign(generateToken.toJSON(), config.SECRET, {
          expiresIn: "30m"
        });
        res.json({ success: true, token: token });
      }
    });
  });

  api.get("/me", checkToken, async (req, res) => {
    const user = await User.findById(req.decoded.user);
    res.status(200).json({ success: true, result: user });
  });

  api.delete("/logout", checkToken, async (req, res) => {
    // console.log(req.decoded._id);

    await Token.findById(req.decoded._id);
    res.send({ success: true, message: "User logout successfuly" });
  });

  return api;
};
