import { Router } from "express";
import multer from "multer";
import Song from "../models/song";
import Singer from "../models/singer";

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./src/public/");
  },
  fileName: (req, file, cb) => {
    console.log(req.body);

    // cb(null, new Date().toString() + file.originalname);\
    cb(null, file.originalname + "-" + Date.now());
  }
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === "audio/mpeg") {
    cb(null, true);
  } else {
    cb(new Error("You can only upload audio/mpeg file"));
  }
};

const mp3 = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 10
  },
  fileFilter: fileFilter
});

export default ({ config, db }) => {
  let api = Router();

  api.get("/", async (req, res) => {
    try {
      const song = await Song.find();
      res.status(200).json({ success: true, data: song });
    } catch (err) {
      res.send(500).json({ success: false, message: err });
    }
  });

  api.post("/addSong", mp3.single("mp3File"), async (req, res) => {
    const singerModel = await Singer.findById({ _id: req.body.singer });
    const newSong = new Song({
      title: req.body.title,
      singer: singerModel._id,
      releaseDate: req.body.releaseDate,
      mp3File: req.file.path,
      createdAt: new Date()
    });

    try {
      let songAdded = await newSong.save();
      res.status(201).json(songAdded);
    } catch (err) {
      console.log(err);

      res.status(400).json(err); //{ message: err.message }
      // 400 - something wrong with user input
    }
  });

  api.put(`/:songId/edit`, mp3.single("mp3File"), async (req, res) => {
    const singerModel = Singer.findById({ _id: req.body.singer });

    Song.findById(req.params.songId, (err, songUpdate) => {
      if (err) {
        res.json(err);
      }

      (songUpdate.title = req.body.title),
        (songUpdate.singer = singerModel._id),
        (songUpdate.releaseDate = req.body.releaseDate),
        (songUpdate.mp3File = req.file.path),
        (songUpdate.updateAt = new Date());
      songUpdate.save(err => {
        if (err) {
          res.json(err);
        }
        res.json({ message: "Song updated" });
      });
    });
  });

  api.delete("/:songId/delete", async (req, res) => {
    await Song.remove(
      {
        _id: req.params.songId
      },
      (err, song) => {
        if (err) {
          res.json(err);
        }
        res.json({ message: "Song successfully deleted" });
      }
    );
  });

  return api;
};
