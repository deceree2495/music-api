const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Singer = require("../models/singer");

const groupBandSchema = new Schema({
  bandName: {
    type: String
  },
  singer: [Singer.schema],
  debutDate: {
    type: Date
  },
  createdAt: {
    type: Date,
    default: null
  },
  updatedAt: {
    type: Date,
    default: null
  },
  deletedAt: {
    type: Date,
    default: null
  },
  assignedAt: {
    type: Date,
    default: null
  }
});

const GroupBand = mongoose.model("GroupBand", groupBandSchema);

module.exports = GroupBand;
