const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const GroupSinger = require("../models/groupSinger");

const songSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  singer: {
    type: Schema.Types.ObjectId,
    ref: "Singer"
  },
  featureSinger: {
    type: Schema.Types.ObjectId,
    ref: "Singer"
  },
  groupSinger: [GroupSinger.schema],

  releaseDate: {
    type: Date,
    required: true
  },
  mp3File: {
    type: String,
    required: true,
    ref: "/public/mp3"
  },
  genre: {
    type: String
    // required: true
  },
  createdAt: {
    type: Date,
    default: null
  },
  updatedAt: {
    type: Date,
    default: null
  },
  deletedAt: {
    type: Date,
    default: null
  },
  assignedAt: {
    type: Date,
    default: null
  }
});

const Song = mongoose.model("Song", songSchema);

module.exports = Song;
