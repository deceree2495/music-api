import mongoose from "mongoose";
// import passportLocalMongoose from "passport-local-mongoose";
const Schema = mongoose.Schema;

const User = new Schema({
  email: {
    type: String,
    unique: true,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  name: {
    firstName: String,
    middleName: String,
    lastName: String
  },
  createdAt: {
    type: Date,
    default: null
  },
  assignedAt: {
    type: Date,
    default: null
  },
  deletedAt: {
    type: Date,
    default: null
  },
  updateAt: {
    type: Date,
    default: null
  }
});

// User.plugin(passportLocalMongoose);

module.exports = mongoose.model("User", User);
