import mongoose from "mongoose";
const Schema = mongoose.Schema;

const Singer = new Schema({
  name: {
    type: String,
    required: true
  },
  groupBand: {
    type: Schema.Types.ObjectId,
    ref: "Singer"
  },
  createdAt: {
    type: Date,
    default: null
  },
  updatedAt: {
    type: Date,
    default: null
  },
  deletedAt: {
    type: Date,
    default: null
  },
  assignedAt: {
    type: Date,
    default: null
  }
});

module.exports = mongoose.model("Singer", Singer);
