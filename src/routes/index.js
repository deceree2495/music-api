import express from "express";
import config from "../config";
import middleware from "../middleware";
import initializeDb from "../db";
import user from "../controller/user";
import singer from "../controller/singer";
import song from "../controller/song";

let router = express();

// connect to db
initializeDb(db => {
  //middleware
  router.use(middleware({ config, db }));

  // routes
  router.use("/user", user({ config, db }));
  router.use("/singer", singer({ config, db }));
  router.use("/song", song({ config, db }));
});

export default router;
